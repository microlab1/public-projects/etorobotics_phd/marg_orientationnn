# -*- coding: utf-8 -*-

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from tensorflow.python.data.ops import dataset_ops
from tensorflow.python.ops import array_ops
from tensorflow.python.ops import math_ops
from tensorflow.python.util.tf_export import keras_export

'''
file = str("../logs/20200706_15_03_03/") # error
file = str("../logs/20200706_15_03_14/") # linear sho 312-123
file = str("../logs/20200706_15_54_06/") # linear mid 59000-23000
file = str("../logs/20200706_16_13_49/") # square mid 10000-3700
file = str("../logs/20200706_16_14_42/") # squrae mid 87000-34000
file = str("../logs/20200706_16_22_17/") # square mid 23000-8900 amorf
file = str("../logs/20200707_11_46_33/") # linear sho 3500-1400
file = str("../logs/20200707_12_32_33/") # error
file = str("../logs/20200707_12_32_38/") # error
file = str("../logs/20200707_12_32_41/") # error
file = str("../logs/20200707_12_34_38/") # error
file = str("../logs/20200707_13_12_55/") # error
file = str("../logs/20200707_13_13_09/") # linear lon 136000-54000
file = str("../logs/20200707_13_25_05/") # linear lon 120000-47000
file = str("../logs/20200707_13_33_27/") # linear lon 120000-46000
file = str("../logs/20200707_13_41_32/") # linear lon 130000-53000
file = str("../logs/20200707_14_44_12/") # infini lon 340000-134000
file = str("../logs/20200707_15_07_14/") # infini lon 370000-150000
'''

file = str("../logs/20200707_11_46_33/")
#file = str("../logs/20200706_16_22_17/")

marg_df  = pd.read_csv(file + "Marg.csv")
mocap_df = pd.read_csv(file + "Mocap.csv")

plot_mocap = mocap_df[0:]
plot_marg  = marg_df[0:]

plot_mocap.plot(kind='scatter', x='Marg_posX', y='Marg_posZ', color='red', label='Position in horizontal plane')
plt.xlabel('MARG_posX [mm]')
plt.ylabel('MARG_posZ [mm]')
plt.show()

plot_marg.plot(x='Timestamp', y='AccX', color='blue', label='AccX')
plot_marg.plot(x='Timestamp', y='AccY', color='green', label='AccY')
plot_marg.plot(x='Timestamp', y='AccZ', color='red', label='AccZ')
plt.xlabel('Time [s]')
plt.ylabel('Acceleration [m/s^2]')
plt.show()

print(marg_df['Timestamp'][0])
print(mocap_df['Timestamp'][0])
print("\n")

marg_header = marg_df.columns.values.tolist()
mocap_header = mocap_df.columns.values.tolist()[1:]

arr_mess  = marg_df.copy().to_numpy()
arr_mocap = mocap_df.copy().to_numpy()
arr_mocap = np.delete(arr_mocap, 0, 0) # timestamp of the first 2 rows are the same

delet_row = [] 
for i in range(0, arr_mess.shape[0]):
    if arr_mess[i,0] < arr_mocap[0,0]:
        delet_row.append(i)
    elif arr_mess[i,0] > arr_mocap[-1,0]:
        delet_row.append(i)
        
arr_mess=np.delete(arr_mess, np.array(delet_row), 0)

extension = np.empty((arr_mess.shape[0],arr_mocap.shape[1]-1,))
extension[:] = np.nan

arr_mess = np.hstack((arr_mess, extension))

idx = 0
for row in range(arr_mess.shape[0]):
    if arr_mess[row, 0] > arr_mocap[idx+1,0]:
        idx = idx+1
    
    for col in  range(arr_mocap.shape[1]-1):
        arr_mess[row, col+10] = np.interp(arr_mess[row, 0], [arr_mocap[idx, 0], arr_mocap[idx+1, 0]], [arr_mocap[idx, col+1], arr_mocap[idx+1, col+1]])
