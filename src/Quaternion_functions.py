# -*- coding: utf-8 -*-
"""
Created on Thu Oct 15 21:04:51 2020

@author: KR
"""

import numpy as np
import sympy
from pyquaternion import Quaternion
import matplotlib.pyplot as plt
import ahrs

#changing the original axis, for possible transformation

# for Y up or for Z up
def w_flip (quat):  
    '''From xyzw flip to wxyz quaternion form.'''
    if (len(quat[0]) != 4):
        print('Not 4 dimensional quat!')
        return
    
    newquat=np.zeros((len(quat),4))
    for i in range(len(quat)):
        newquat[i] = [quat[i,3], quat[i,0], quat[i,1], quat[i,2]] #x,y,z,w -> w,x,y,z
    return newquat

def w_back_flip (quat): 
    '''From wxyz flip to xyzw quaternion form.'''
    if (len(quat[0]) != 4):
        print('Not 4 dimensional quat!')
        return
    
    newquat=np.zeros((len(quat),4))
    for i in range(len(quat)):
        newquat[i] = [quat[i,1], quat[i,2], quat[i,3], quat[i,0]] #x,y,z,w -> w,x,y,z
    return newquat

def MOC_to_MAD (quat):# mocapnek oda   Madgwicknek vissza   
    '''From wxyz to wy-x-z '''
    if (len(quat[0]) != 4):
        print('not 4 dimensional quat!')
        return
    
    newquat=np.zeros((len(quat),4))
    for i in range(len(quat)):
        newquat[i] = [quat[i,0], quat[i,3], -1*quat[i,1], -1*quat[i,2]] #w,x,y,z -> w,z,-x,-y updated
    return newquat

def MAD_to_MOC (quat):#  mocapnek vissza   Madgwicknek oda
    '''From wxyz to w-yx-z'''
    if (len(quat[0]) != 4):
        print('not 4 dimensional quat!')
        return
    
    newquat=np.zeros((len(quat),4))
    for i in range(len(quat)):
        newquat[i] = [quat[i,0], -1*quat[i,2], -1*quat[i,3], quat[i,1]] #w,x,y,z -> w,-y,-z,x updated
    return newquat
    
#=============================================================================
# The important functions for the transformation

def qprod(a, b):
    '''Quaternion multiplication.'''
    w=a[0]*b[0]-a[1]*b[1]-a[2]*b[2]-a[3]*b[3]
    x=a[0]*b[1]+a[1]*b[0]+a[2]*b[3]-a[3]*b[2]
    y=a[0]*b[2]+a[2]*b[0]-a[1]*b[3]+a[3]*b[1]
    z=a[0]*b[3]+a[3]*b[0]+a[1]*b[2]-a[2]*b[1]
    q = np.zeros(4)
    q[0] = w
    q[1] = x
    q[2] = y
    q[3] = z

    return q

def invq(a):
    w = a[0]
    x = a[1]* -1
    y = a[2]* -1
    z = a[3]* -1
    q = np.zeros(4)
    q[0] = w
    q[1] = x
    q[2] = y
    q[3] = z
    
    return q
    


def pointprod(q, p): 
    '''Quaternion multiplication with a point: q(p)q^-1'''
    w=-q[1]*p[0]-q[2]*p[1]-q[3]*p[2]
    x=q[0]*p[0]+q[2]*p[2]-q[3]*p[1]
    y=q[0]*p[1]-q[1]*p[2]+q[3]*p[0]
    z=q[0]*p[2]+q[1]*p[1]-q[2]*p[0] #q*p
    
    qp=[w,x,y,z] # = q*p
    qi=[q[0],-q[1],-q[2],-q[3]] # -q
    pnew= qprod(qp,qi) #q*p *-q
    
    pp=[pnew[1],pnew[2],pnew[3]]
    
    return pp


def transform(quat1, quat2, axis, n): 
    ''' Transform quat2 TO quat1 (moc <- mad)(to <- what) '''
    solution = np.zeros((n,4))
    for i in range(n): #axis = melyiken cseréltük a tengelyeit
        w= sympy.Symbol('w')
        x= sympy.Symbol('x')  
        y= sympy.Symbol('y')
        z= sympy.Symbol('z')
        
        transform= [w,x,y,z]
        
        if (axis == "MOC"):
            prod = qprod(quat2[i], transform)
        if (axis == "MAD"):
            prod = qprod(transform, quat2[i])    
        
        
        eq1=prod[0]-quat1[i][0]
        eq2=prod[1]-quat1[i][1]
        eq3=prod[2]-quat1[i][2]
        eq4=prod[3]-quat1[i][3]
    
        sol=(sympy.nsolve((eq1,eq2,eq3,eq4), (w,x,y,z),(1,1,1,1),verify=False))
        
        solution[i]=[sol[0],sol[1],sol[2],sol[3]]
    
    transform = np.average(solution, axis=0)
    return transform



def turn(transform, madgwick, axis):  #mivel mit transzformálunk
    '''
    axis = "MOC" or "MAD"
    '''
    if (axis == "MOC"):
        target = qprod(madgwick, transform)
    if (axis == "MAD"):    
        target = qprod(transform, madgwick)
    
    return target

def distance_check(quat1, quat2, dlist = False, plot = True):
    
    if (len(quat1) != len(quat2)):
        print ('nem egyezik a vektorok hossza!')
        return
    if (len(quat1[0]) != 4 or len(quat2[0]) != 4):
        print ('Not a 4 element quaternion!')
        return
    
    distance=np.zeros(len(quat1))
    time=np.zeros(len(quat1))
    
    for i in range(len(quat1)):
        time[i]=0.004*i
        
        q1 = Quaternion(quat1[i][0],quat1[i][1],quat1[i][2],quat1[i][3])
        q2 = Quaternion(quat2[i][0],quat2[i][1],quat2[i][2],quat2[i][3])
        
        distance[i]=Quaternion.distance(q1,q2)
    
    if (plot == True):
        plt.title("distance between the Mocap and Madgwick")
        plt.plot(time,distance,label='distance')
        plt.xlabel('time [-]')
        plt.ylabel('quaternion distance [-]')
        #plt.legend()
    
    if(dlist == True):
        return distance

def distance (quat1, quat2):
    
    if (len(quat1[0]) !=1 or len(quat2[0]) != 1):
        print ('nem jók a vektorok !')
        return
    if (len(quat1) != 4 or len(quat2) != 4):
        print ('nem 4 hosszú quaterniok!')
        return
    
    q1 = Quaternion(quat1[0],quat1[1],quat1[2],quat1[3])
    q2 = Quaternion(quat2[0],quat2[1],quat2[2],quat2[3])
        
    distance=Quaternion.distance(q1,q2)
    return distance

def mocap_correction(quat, plot=True):
    
    distance = np.zeros(len(quat)-1)
    time = np.zeros(len(quat)-1)

    e=1
    for i in range(len(quat)-1): # az egész adathalmazra nézve megézi a quaternio távolságokat
        time[i] = i*0.004
        
        q1 = Quaternion(quat[i][0],quat[i][1],quat[i][2],quat[i][3])
        q2 = Quaternion(quat[i+1][0],quat[i+1][1],quat[i+1][2],quat[i+1][3])
        
        distance[i]=Quaternion.distance(q1,q2)
        quat[i]*=e
        if (distance[i] >0.30) :
            e *= -1
    quat[-1]*=e

    if (plot==True):
        plt.title("distance between two quaternions of Madgwick")
        plt.plot(time,distance,label='distance')
        plt.xlabel('time [-]')
        plt.ylabel('quaternion distance [-]')
        plt.legend()
        
    
    return quat

def quaternion_to_euler_angle_vectorized1(w, x, y, z):
    ysqr = y * y

    t0 = +2.0 * (w * x + y * z)
    t1 = +1.0 - 2.0 * (x * x + ysqr)
    X = np.degrees(np.arctan2(t0, t1))

    t2 = +2.0 * (w * y - z * x)
    t2 = np.where(t2>+1.0,+1.0,t2)
    #t2 = +1.0 if t2 > +1.0 else t2

    t2 = np.where(t2<-1.0, -1.0, t2)
    #t2 = -1.0 if t2 < -1.0 else t2
    Y = np.degrees(np.arcsin(t2))

    t3 = +2.0 * (w * z + x * y)
    t4 = +1.0 - 2.0 * (ysqr + z * z)
    Z = np.degrees(np.arctan2(t3, t4))

    return X, Y, Z 

def compare_mocap_madgwick(datastream,  plot_start, plot_end, fast_conv = 60, firstq = 100, base='mocap', mode='MARG'):
    
    datastream.filterData(31, 'hanning', [1, 2, 3]) # filter Acc
    datastream.filterData(31, 'hanning', [4, 5, 6]) # filter Gyro
    datastream.filterData(31, 'hanning', [7, 8, 9]) # filter Mag
    #d.plotData(acc=True,gyro=True,mag=True)
    
    Acc    = datastream.data[:,1:4]
    Gyro   = datastream.data[:,4:7]
    Mag    = -1 *datastream.data[:,7:10]
    #Mag[:, [0, 1, 2]] = Mag[:, [1, 0, 2]]
    MocapQ = datastream.data[:,13:17]
    MocapE = datastream.data[:,17:20]
    
    num_samples = datastream.data.shape[0]
    
    madgwick = ahrs.filters.Madgwick()
    madgwick.Dt = 0.004
    madgwick.gain = 2.5
    madgwick.q0 = Mag[0]
    
    Q = np.tile([1.,0.,0.,0.], (num_samples, 1)) # allocate quaternios
    
    
    for t in range(1, num_samples):
        if (t == fast_conv):
            madgwick.gain = 0.041
        if (mode == 'IMU'):
            Q[t] = madgwick.updateMARG(Q[t-1], acc=Acc[t]*9.8077, gyr=Gyro[t]/57.3,  mag=Mag[t]*0)
        if (mode == 'MARG'):
            Q[t] = madgwick.updateMARG(Q[t-1], acc=Acc[t]*9.8077, gyr=Gyro[t]/57.3, mag=Mag[t]*10)
    
    if (mode == 'IMU'):
        moc = MocapQ[0]
        mad = np.array([1.,0.,0.,0.])
    if (mode == 'MARG'):
        moc = MocapQ[firstq]
        mad = Q[firstq]
    
    moc = invq(moc)
    if (mode == 'MARG'):
        mad = invq(mad)

    for i in range(len(Q)):
        MocapQ[i] = qprod(MocapQ[i],moc)
        if (mode == 'MARG'):
                Q[i] = qprod(Q[i],mad)
       
    plt.figure(3, figsize=(15, 13))
    plt.subplot(221)
    plt.title("Madgwick_Mocap")
    plt.plot(MocapQ[plot_start:plot_end,0], color='blue', label='MocapQ0')
    plt.plot(Q[plot_start:plot_end,0], color='blue', label='Madgwic0',linestyle= '--')
    plt.xlabel('Iteration [-]')
    plt.ylabel('Amplitude [-]')
    plt.legend()
    
    plt.subplot(222)
    plt.title("Madgwick_Mocap")
    plt.plot(MocapQ[plot_start:plot_end,1], color='red', label='MocapQ1')
    plt.plot(Q[plot_start:plot_end,1], color='red', label='Madgwic1',linestyle='--')
    plt.xlabel('Iteration [-]')
    plt.ylabel('Amplitude [-]')
    plt.legend()
    
    plt.subplot(223)
    plt.title("Madgwick_Mocap")
    plt.plot(MocapQ[plot_start:plot_end,2], color='green', label='MocapQ2')
    plt.plot(Q[plot_start:plot_end,2], color='green', label='Madgwic2',linestyle= '--')
    plt.xlabel('Iteration [-]')
    plt.ylabel('Amplitude [-]')
    plt.legend()
    
    plt.subplot(224)
    plt.title("Madgwick_Mocap")
    plt.plot(MocapQ[plot_start:plot_end,3], color='orange', label='MocapQ3')
    plt.plot(Q[plot_start:plot_end,3], color='orange', label='Madgwic3',linestyle= '--')
    plt.xlabel('Iteration [-]')
    plt.ylabel('Amplitude [-]')
    plt.legend()
    
    plt.show()
    
    return Q, MocapQ, moc, mad

def transform_matrix(Q, MocapQ, moc, mad,  plot_start, plot_end, f, s, t, base='mocap', mode='MARG'):
    print('first step: generation and starting point')
    
    #distance_check(Q, MocapQ)
    print('help, to determine f s t')
    '''
    f = 800
    s = 1750
    t = 2600
    '''
    mad1 = Q[f,1:]
    mad2 = Q[s,1:]
    mad3 = Q[t,1:]

    moc1 = MocapQ[f,1:]
    moc2 = MocapQ[s,1:]
    moc3 = MocapQ[t,1:]

    a = sympy.Symbol('a')
    b = sympy.Symbol('b')
    c = sympy.Symbol('c')
    d = sympy.Symbol('d')
    e = sympy.Symbol('e')
    f = sympy.Symbol('f')
    g = sympy.Symbol('g')
    h = sympy.Symbol('h')
    i = sympy.Symbol('i')
    
    #[a,e,i
    # d,b,f
    # g,h,c]
    
    if(base=='mocap'):
    
        eq1 = mad1[0] * a + mad1[1] * b + mad1[2] * c - moc1[0]
        eq2 = mad1[0] * d + mad1[1] * e + mad1[2] * f - moc1[1]
        eq3 = mad1[0] * g + mad1[1] * h + mad1[2] * i - moc1[2]
        
        eq4 = mad2[0] * a + mad2[1] * b + mad2[2] * c - moc2[0]
        eq5 = mad2[0] * d + mad2[1] * e + mad2[2] * f - moc2[1]
        eq6 = mad2[0] * g + mad2[1] * h + mad2[2] * i - moc2[2]
        
        eq7 = mad3[0] * a + mad3[1] * b + mad3[2] * c - moc3[0]
        eq8 = mad3[0] * d + mad3[1] * e + mad3[2] * f - moc3[1]
        eq9 = mad3[0] * g + mad3[1] * h + mad3[2] * i - moc3[2]
        
    elif(base=='madgwick'):
        
        eq1 = moc1[0] * a + moc1[1] * b + moc1[2] * c - mad1[0]
        eq2 = moc1[0] * d + moc1[1] * e + moc1[2] * f - mad1[1]
        eq3 = moc1[0] * g + moc1[1] * h + moc1[2] * i - mad1[2]
        
        eq4 = moc2[0] * a + moc2[1] * b + moc2[2] * c - mad2[0]
        eq5 = moc2[0] * d + moc2[1] * e + moc2[2] * f - mad2[1]
        eq6 = moc2[0] * g + moc2[1] * h + moc2[2] * i - mad2[2]
        
        eq7 = moc3[0] * a + moc3[1] * b + moc3[2] * c - mad3[0]
        eq8 = moc3[0] * d + moc3[1] * e + moc3[2] * f - mad3[1]
        eq9 = moc3[0] * g + moc3[1] * h + moc3[2] * i - mad3[2]
        
       
        
    sol=(sympy.nsolve((eq1,eq2,eq3,eq4,eq5,eq6,eq7,eq8,eq9), (a,b,c,d,e,f,g,h,i),(0,0,0,0,0,0,0,0,0),verify=False))
  
    
    solution = np.array([[sol[0], sol[1], sol[2]], [sol[3], sol[4], sol[5]], [sol[6], sol[7], sol[8]]])
    print(solution)
    
   
    if (base == 'mocap'):
        #moc = invq(moc)
        
        for i in range(len(Q)):
            Q[i, 1:] = np.matmul(solution, Q[i, 1:]) #transformin the x,y,z axis
            Q[i] = qprod(Q[i],moc)
            MocapQ[i] = qprod(MocapQ[i],moc)
    
    elif (base == 'madgwick'):
        if (mode == 'MARG'):
            mad = invq(mad)
        
        for i in range(len(MocapQ)):
            MocapQ[i, 1:] = np.matmul(solution, MocapQ[i, 1:]) #transformin the x,y,z axis
            if (mode == 'MARG'):
                Q[i] = qprod(Q[i],mad)
                MocapQ[i] = qprod(MocapQ[i],mad)

    plt.figure(3, figsize=(15, 13))
    plt.subplot(221)
    plt.title("Madgwick_Mocap")
    plt.plot(MocapQ[plot_start:plot_end,0], color='blue', label='MocapQ0')
    plt.plot(Q[plot_start:plot_end,0], color='blue', label='Madgwic0',linestyle= '--')
    plt.xlabel('Iteration [-]')
    plt.ylabel('Amplitude [-]')
    plt.legend()
    
    plt.subplot(222)
    plt.title("Madgwick_Mocap")
    plt.plot(MocapQ[plot_start:plot_end,1], color='red', label='MocapQ1')
    plt.plot(Q[plot_start:plot_end,1], color='red', label='Madgwic1',linestyle='--')
    plt.xlabel('Iteration [-]')
    plt.ylabel('Amplitude [-]')
    plt.legend()
    
    plt.subplot(223)
    plt.title("Madgwick_Mocap")
    plt.plot(MocapQ[plot_start:plot_end,2], color='green', label='MocapQ2')
    plt.plot(Q[plot_start:plot_end,2], color='green', label='Madgwic2',linestyle= '--')
    plt.xlabel('Iteration [-]')
    plt.ylabel('Amplitude [-]')
    plt.legend()
    
    plt.subplot(224)
    plt.title("Madgwick_Mocap")
    plt.plot(MocapQ[plot_start:plot_end,3], color='orange', label='MocapQ3')
    plt.plot(Q[plot_start:plot_end,3], color='orange', label='Madgwic3',linestyle= '--')
    plt.xlabel('Iteration [-]')
    plt.ylabel('Amplitude [-]')
    plt.legend()
    
    
    plt.show()
    
    return Q, MocapQ

def madgwick_turn(Q, array):
    tmp = np.zeros((len(Q), 4))
    pre = np.array([1,1,1])
    if (array[0]<0):
        pre[0] = -1
        array[0] = abs(array[0])
    if (array[1]<0):
        pre[1] = -1
        array[1] = abs(array[1])
    if (array[2]<0):
        pre[2] = -1
        array[2] = abs(array[2])
    for i in range(len(Q)):
        tmp[i] = [Q[i,0], pre[0]*Q[i,array[0]], pre[1]*Q[i,array[1]], pre[2]*Q[i,array[2]]]
    return tmp

def finish(Q, MocapQ, moc, mad, base='mocap', mode='MARG'):
    
    if (base == 'mocap'):
        moc = invq(moc)
        
        for i in range(len(Q)):
            Q[i] = qprod(Q[i],moc)
            MocapQ[i] = qprod(MocapQ[i],moc)
    
    elif (base == 'madgwick'):
        if (mode == 'MARG'):
            mad = invq(mad)
        
        for i in range(len(MocapQ)):
            if (mode == 'MARG'):
                Q[i] = qprod(Q[i],mad)
                MocapQ[i] = qprod(MocapQ[i],mad)
    
    return Q, MocapQ
    
