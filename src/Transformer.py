import tensorflow as tf
import time
import numpy as np
import pickle


def get_angles(pos, i, d_model):
    angle_rates = 1 / np.power(10000, (2 * (i // 2)) / np.float32(d_model))
    return pos * angle_rates


def positional_encoding(position, d_model):
    angle_rads = get_angles(np.arange(position)[:, np.newaxis],
                            np.arange(d_model)[np.newaxis, :],
                            d_model)
    angle_rads[:, 0::2] = np.sin(angle_rads[:, 0::2])
    angle_rads[:, 1::2] = np.cos(angle_rads[:, 1::2])

    pos_encoding = angle_rads[np.newaxis, ...]
    return tf.cast(pos_encoding, dtype=tf.float32)


def create_look_ahead_mask(size):
    mask = 1 - tf.linalg.band_part(tf.ones((size, size)), -1, 0)
    return mask


def rotate(matrix, shifts):
    """"requested rotate function - assumes matrix shape is mxn and shifts shape is m"""

    # get shape of the input matrix
    shape = tf.shape(matrix)

    # compute and stack the meshgrid to get the index matrix of shape (2,m,n)
    ind = tf.stack(tf.meshgrid(tf.range(shape[0]), tf.range(shape[1]), indexing='ij'))

    # reshape it to (m,n,2)
    ind = tf.transpose(ind, [1, 2, 0])

    # add the value from shifts to the corresponding row and devide modulo shape[1]
    # this will effectively introduce the desired shift, but at the level of indices
    shifted_ind = tf.math.mod(tf.transpose(tf.transpose(ind[:, :, 1]) + shifts), shape[1])

    # convert the shifted indices to the right shape
    new_ind = tf.transpose(tf.stack([ind[:, :, 0], shifted_ind]), [1, 2, 0])

    # return the resliced tensor
    return tf.gather_nd(matrix, new_ind)


def create_log_sparse_mask(size, local, logparts):
    intlen = (size - local) // logparts
    logn = tf.cast(tf.math.floor(tf.math.log(tf.cast(intlen, tf.float32)) / tf.math.log(2.0)), tf.int32)

    idx = tf.reverse(
        tf.cast(tf.reshape(tf.math.pow(2, int(logn) - tf.range(logn + 1, dtype=tf.int32)) - 1, [logn + 1, 1]),
                tf.int64), [0])
    idx = tf.reshape(tf.stack([idx, tf.zeros([logn + 1, 1], dtype=tf.int64)], axis=1), [logn + 1, 2])
    x = tf.sparse.SparseTensor(idx, tf.ones(logn + 1, dtype=tf.int32), dense_shape=[intlen, 1])
    x = tf.reverse(tf.sparse.to_dense(x), [0])
    x = tf.transpose(tf.concat([tf.tile(x, [logparts, 1]), tf.ones([local, 1], dtype=tf.int32)], axis=0))

    z = tf.zeros([size, size], dtype=tf.int32)
    q = z + tf.reshape(tf.range(size), [size, 1])

    k = z + tf.reshape(tf.range(size), [1, size])
    c1 = q >= k
    c2 = tf.cast(rotate(z + x, size - 1 - 1 * tf.range(size)), tf.bool)
    c3 = tf.logical_and(c1, c2)

    return 1.0 - tf.cast(c3, tf.float32)


def scaled_dot_product_attention(q, k, v, mask):
    """Calculate the attention weights.
    q, k, v must have matching leading dimensions.
    k, v must have matching penultimate dimension, i.e.: seq_len_k = seq_len_v.
    The mask has different shapes depending on its type(padding or look ahead)
    but it must be broadcastable for addition.

    Args:
    q: query shape == (..., seq_len_q, depth)
    k: key shape == (..., seq_len_k, depth)
    v: value shape == (..., seq_len_v, depth_v)
    mask: Float tensor with shape broadcastable
          to (..., seq_len_q, seq_len_k). Defaults to None.

    Returns:
    output, attention_weights
    """

    matmul_qk = tf.matmul(q, k, transpose_b=True)

    dk = tf.cast(tf.shape(k)[-1], tf.float32)
    scaled_attention_logits = matmul_qk / tf.math.sqrt(dk)

    if mask is not None:
        scaled_attention_logits += (
                mask * -1e9)  # softmax az 1esek helyen kozel vegtelen negativ inputra kozel nulla outputot ad majd

    attention_weights = tf.nn.softmax(scaled_attention_logits, axis=-1)

    output = tf.matmul(attention_weights, v)

    return output, attention_weights


def print_out(q, k, v):
    temp_out, temp_attn = scaled_dot_product_attention(q, k, v, None)
    print('Attention weights are:')
    print(temp_attn)
    print('Output is:')
    print(temp_out)


class CausalConvMultiHeadAttention(tf.keras.layers.Layer):
    def __init__(self, d_model, kernel_size, num_heads):
        super(CausalConvMultiHeadAttention, self).__init__()
        self.num_heads = num_heads
        self.d_model = d_model

        assert d_model % self.num_heads == 0

        self.depth = d_model // self.num_heads

        self.wq = tf.keras.layers.Conv1D(d_model, kernel_size, padding="causal")
        self.wk = tf.keras.layers.Conv1D(d_model, kernel_size, padding="causal")
        self.wv = tf.keras.layers.Conv1D(d_model, 1, padding="same")

        self.dense = tf.keras.layers.Dense(d_model)

    def split_heads(self, x, batch_size):
        x = tf.reshape(x, (batch_size, -1, self.num_heads, self.depth))
        return tf.transpose(x, perm=[0, 2, 1, 3])

    def call(self, v, k, q, mask):
        batch_size = tf.shape(q)[0]

        q = self.wq(q)  # (batch_size, seq_len, d_model)
        k = self.wk(k)  # (batch_size, seq_len, d_model)
        v = self.wv(v)  # (batch_size, seq_len, d_model)

        q = self.split_heads(q, batch_size)  # (batch_size, num_heads, seq_len_q, depth)
        k = self.split_heads(k, batch_size)  # (batch_size, num_heads, seq_len_q, depth)
        v = self.split_heads(v, batch_size)  # (batch_size, num_heads, seq_len_q, depth)

        scaled_attention, attention_weights = scaled_dot_product_attention(q, k, v, mask)

        scaled_attention = tf.transpose(scaled_attention,
                                        perm=[0, 2, 1, 3])  # (batch_size, seq_len_q, num_heads, depth)
        concat_attention = tf.reshape(scaled_attention, (batch_size, -1, self.d_model))

        output = self.dense(concat_attention)  # (batch_size, seq_len_q, d_model)
        return output, attention_weights


def point_wise_feed_forward_network(d_model, dff):
    return tf.keras.Sequential([
        tf.keras.layers.Dense(dff, activation="relu"),  # (batch_size, seq_len, dff)
        tf.keras.layers.Dense(d_model)  # (batch_size, seq_len, d_model)
    ])


class EncoderLayer(tf.keras.layers.Layer):
    def __init__(self, d_model, kernel_size, num_heads, dff, rate=0.1):
        super(EncoderLayer, self).__init__()

        self.mha = CausalConvMultiHeadAttention(d_model, kernel_size, num_heads)
        self.ffn = point_wise_feed_forward_network(d_model, dff)

        self.layernorm1 = tf.keras.layers.LayerNormalization(epsilon=1e-6)
        self.layernorm2 = tf.keras.layers.LayerNormalization(epsilon=1e-6)

        self.dropout1 = tf.keras.layers.Dropout(rate)
        self.dropout2 = tf.keras.layers.Dropout(rate)

    def call(self, x, training, mask):
        attn_output, _ = self.mha(x, x, x, mask)  # (batch_size, input_seq_len, d_model)
        attn_output = self.dropout1(attn_output, training=training)
        out1 = self.layernorm1(x + attn_output)  # (batch_size, input_seq_len, d_model)

        ffn_output = self.ffn(out1)  # (batch_size, input_seq_len, d_model)
        ffn_output = self.dropout2(ffn_output, training=training)
        out2 = self.layernorm2(out1 + ffn_output)  # (batch_size, input_seq_len, d_model)

        return out2


class DecoderLayer(tf.keras.layers.Layer):
    def __init__(self, d_model, kernel_size, num_heads, dff, rate=0.1):
        super(DecoderLayer, self).__init__()

        self.mha1 = CausalConvMultiHeadAttention(d_model, kernel_size, num_heads)
        self.mha2 = CausalConvMultiHeadAttention(d_model, kernel_size, num_heads)

        self.ffn = point_wise_feed_forward_network(d_model, dff)

        self.layernorm1 = tf.keras.layers.LayerNormalization(epsilon=1e-6)
        self.layernorm2 = tf.keras.layers.LayerNormalization(epsilon=1e-6)
        self.layernorm3 = tf.keras.layers.LayerNormalization(epsilon=1e-6)

        self.dropout1 = tf.keras.layers.Dropout(rate)
        self.dropout2 = tf.keras.layers.Dropout(rate)
        self.dropout3 = tf.keras.layers.Dropout(rate)

    def call(self, x, enc_output, training, look_ahead_mask):
        attn1, attn_weights_block1 = self.mha1(x, x, x, look_ahead_mask)  # (batch_size, target_seq_len, d_model)
        attn1 = self.dropout1(attn1, training=training)
        out1 = self.layernorm1(attn1 + x)

        attn2, attn_weights_block2 = self.mha2(enc_output, enc_output, out1,
                                               None)  # (batch_size, target_seq_len, d_model)
        attn2 = self.dropout2(attn2, training=training)
        out2 = self.layernorm2(attn2 + out1)  # (batch_size, target_seq_len, d_model)

        ffn_output = self.ffn(out2)  # (batch_size, target_seq_len, d_model)
        ffn_output = self.dropout3(ffn_output, training=training)
        out3 = self.layernorm3(ffn_output + out2)  # (batch_size, target_seq_len, d_model)

        return out3, attn_weights_block1, attn_weights_block2


class Encoder(tf.keras.layers.Layer):
    def __init__(self, num_layers, d_model, kernel_size, num_heads, dff,
                 maximum_position_encoding, rate=0.1):
        super(Encoder, self).__init__()

        self.d_model = d_model
        self.num_layers = num_layers

        self.redimension_layer = tf.keras.layers.Conv1D(d_model, 1, padding="same")

        self.pos_encoding = positional_encoding(maximum_position_encoding,
                                                self.d_model)
        self.enc_layers = [EncoderLayer(d_model, kernel_size, num_heads, dff, rate)
                           for _ in range(num_layers)]

        self.dropout = tf.keras.layers.Dropout(rate)

    def call(self, x, training, mask):
        seq_len = tf.shape(x)[1]

        x = self.redimension_layer(x)
        # (batch_size, input_seq_len, d_model)

        # Maybe: x *= tf.math.sqrt(tf.cast(self.d_model, tf.float32))
        x  # = tf.reshape(x,(64, 128, self.d_model)) # seq_len

        # print('Print encoder shape:' + str(x.shape))

        x += self.pos_encoding[:, :seq_len, :]

        x = self.dropout(x, training=training)

        for i in range(self.num_layers):
            x = self.enc_layers[i](x, training, mask)

        return x


class Decoder(tf.keras.layers.Layer):
    def __init__(self, num_layers, d_model, kernel_size, num_heads, dff,
                 maximum_position_encoding, rate=0.1):
        super(Decoder, self).__init__()

        self.d_model = d_model
        self.num_layers = num_layers

        self.pos_encoding = positional_encoding(maximum_position_encoding, d_model)

        self.redimension_layer = tf.keras.layers.Conv1D(d_model, 1, padding="same")

        self.dec_layers = [DecoderLayer(d_model, kernel_size, num_heads, dff, rate)
                           for _ in range(num_layers)]
        self.dropout = tf.keras.layers.Dropout(rate)

    def call(self, x, enc_output, training, look_ahead_mask):
        attention_weights = {}
        seq_len = tf.shape(x)[1]

        x = self.redimension_layer(x)
        # (batch_size, target_seq_len, d_model)

        # Maybe:         x*=tf.math.sqrt(tf.cast(self.d_model, tf.float32))
        # Maybe:         x = tf.reshape(x,(64, 2, self.d_model))

        # print('Print decoder shape:' + str(x.shape))

        x += self.pos_encoding[:, :seq_len, :]

        x = self.dropout(x, training=training)

        for i in range(self.num_layers):
            x, block1, block2 = self.dec_layers[i](x, enc_output, training,
                                                   look_ahead_mask)

            attention_weights['decoder_layer{}_block1'.format(i + 1)] = block1
            attention_weights['decoder_layer{}_block2'.format(i + 1)] = block2

        # x.shape == (batch_size, target_seq_len, d_model)
        return x, attention_weights


class CustomSchedule(tf.keras.optimizers.schedules.LearningRateSchedule):
    def __init__(self, d_model, warmup_steps=4000):
        super(CustomSchedule, self).__init__()

        self.d_model = d_model
        self.d_model = tf.cast(self.d_model, tf.float32)

        self.warmup_steps = warmup_steps

    def __call__(self, step):
        arg1 = tf.math.rsqrt(step)
        arg2 = step * (self.warmup_steps ** -1.5)

        return tf.math.rsqrt(self.d_model) * tf.math.minimum(arg1, arg2)


class FinalLayerStuctured(tf.keras.layers.Layer):
    def __init__(self, output_size):
        super(FinalLayerStuctured, self).__init__()

        self.gap = tf.keras.layers.Flatten()
        self.dense1 = tf.keras.layers.Dense(output_size * 2)
        self.dense2 = tf.keras.layers.Dense(output_size)

    def call(self, x):
        x = self.gap(x)
        x = self.dense1(x)
        x = self.dense2(x)
        return x


class FinalLayerStucturedGAP(tf.keras.layers.Layer):
    def __init__(self, output_size):
        super(FinalLayerStucturedGAP, self).__init__()

        self.gap = tf.keras.layers.GlobalAveragePooling1D()
        self.dense = tf.keras.layers.Dense(output_size)

    def call(self, x):
        x = self.gap(x)
        x = self.dense(x)
        return x


class TransformerModel(tf.keras.Model):
    def __init__(self, num_layers, d_model, kernel_size, num_heads, dff, output_size, input_seq_len, target_seq_len,
                 rate=0.1):
        # pe = positional encoding pe_target - decoder bementánek a hossza is ez legyen idősorban (decoder bemnet length)
        super(TransformerModel, self).__init__()
        self.encoder = Encoder(num_layers, d_model, kernel_size, num_heads, dff, input_seq_len, rate)
        self.decoder = Decoder(num_layers, d_model, kernel_size, num_heads, dff, target_seq_len, rate)

        # Avrega poolinsg és további rétegekkel kontrollálni a hosszt
        self.final_layer = FinalLayerStucturedGAP(output_size)
        # self.final_layer = FinalLayerStuctured(output_size)

    def call(self, inp, tar, training, enc_mask, dec_mask):
        enc_output = self.encoder(inp, training, enc_mask)

        dec_output, attention_weights = self.decoder(tar, enc_output, training,
                                                     dec_mask)
        final_output = self.final_layer(dec_output)

        return final_output, attention_weights, enc_output


class Transformer():
    def __init__(self, num_layers, d_model, kernel_size, num_heads, dff, encoder_size, decoder_size, output_size,
                 input_seq_len, target_seq_len,
                 rate=0.1):

        self.num_layers = num_layers
        self.d_model=d_model
        self.kernel_size=kernel_size
        self.num_heads=num_heads
        self.dff=dff
        self.rate = rate

        self.model = TransformerModel(num_layers, d_model, kernel_size, num_heads, dff, output_size, input_seq_len,
                                      target_seq_len, rate=rate)

        self.input_seq_len = input_seq_len
        self.input_size = encoder_size
        self.target_seq_len = target_seq_len
        self.target_size = decoder_size

        self.history = {"train": [], "val": []}

        self.output_size = output_size

        self.train_step_signature = [
            tf.TensorSpec(shape=(None, input_seq_len, encoder_size), dtype=tf.float32),
            tf.TensorSpec(shape=(None, target_seq_len, decoder_size), dtype=tf.float32),
            tf.TensorSpec(shape=(None, output_size), dtype=tf.float32)
        ]

    def setLoss(self, lossfunction):
        self.loss_function = lossfunction
        self.train_loss = tf.keras.metrics.Mean(name='train_loss')
        self.val_loss = tf.keras.metrics.Mean(name='val_loss')

    def setOptimizer(self, optimizer):
        self.optimizer = optimizer

    def create_masks(self, inp, tar, elocal, elogparts, dlocal, dlogparts):
        # look_ahead_mask = create_look_ahead_mask(tf.shape(tar)[1])
        # print(tf.shape(tar)[1])
        enc_mask = create_log_sparse_mask(tf.shape(inp)[1], elocal, elogparts)
        dec_mask = create_log_sparse_mask(tf.shape(tar)[1], dlocal, dlogparts)
        return enc_mask, dec_mask

    def setEncoderLogsparseMasks(self, local, repetitions):
        self.elocal = local
        self.erepetitions = repetitions

    def setDecoderLogsparseMasks(self, local, repetitions):
        self.dlocal = local
        self.drepetitions = repetitions

    def predict(self, X_enc, X_dec):
        # dims: batchsize, seq_len, input_dims
        enc_mask, dec_mask = self.create_masks(X_enc, X_dec, self.elocal, self.erepetitions, self.dlocal,
                                               self.drepetitions)
        prediction, attention_weights, enc_output = self.model(X_enc, X_dec, False, enc_mask, dec_mask)

        return prediction, attention_weights, enc_output

    def predict_structured(self, X_enc, X_dec_init):
        # X_enc dims: samplenum, seq_len, input_dims
        # X_dec_init dims: 1, seq_len, output_dims
        predlen = X_enc.shape[0]
        output_dim = X_dec_init.shape[2]

        xshape = list(X_enc.shape)
        xshape[0] = 1

        yshape = list(X_dec_init.shape)
        yshape[0] = 1

        # indexeles biztonsagbol
        X_dec = X_dec_init[0, :, :].reshape(yshape)

        predictions = np.zeros((predlen, output_dim))
        for t in range(predlen):
            pred, _, _ = self.predict(X_enc[t, ...].reshape(xshape), X_dec)
            pnumpy = pred.numpy()
            predictions[t, :] = pnumpy.copy()
            X_dec[0, :-1, :] = X_dec[0, 1:, :].copy()
            X_dec[0, -1, :] = pnumpy.copy()

            if t % (predlen // 10) == 0:
                print(int(np.round(t / predlen * 100)), "%")

        return predictions

    def train(self, epochs, X_enc, X_dec, Y_dec, validation_data=None, shuffle=False):

        @tf.function(input_signature=self.train_step_signature)
        def train_step(enc_inp, dec_inp, dec_out):

            enc_mask, dec_mask = self.create_masks(enc_inp, dec_inp, self.elocal, self.erepetitions, self.dlocal,
                                                   self.drepetitions)

            with tf.GradientTape() as tape:
                predictions, _, _ = self.model(enc_inp, dec_inp, True, enc_mask, dec_mask)

                loss = self.loss_function(dec_out, predictions)

            gradients = tape.gradient(loss, self.model.trainable_variables)
            self.optimizer.apply_gradients(zip(gradients, self.model.trainable_variables))

            self.train_loss(loss)

        @tf.function(input_signature=self.train_step_signature)
        def validation_step(enc_inp, dec_inp, dec_out):

            enc_mask, dec_mask = self.create_masks(enc_inp, dec_inp, self.elocal, self.erepetitions, self.dlocal,
                                                   self.drepetitions)

            predictions, _, _ = self.model(enc_inp, dec_inp, True, enc_mask, dec_mask)

            loss = self.loss_function(dec_out, predictions)
            self.val_loss(loss)

        for epoch in range(epochs):
            stime = time.time()

            self.train_loss.reset_states()
            self.val_loss.reset_states()

            if shuffle:
                batch_idx = np.random.permutation(X_enc.shape[0])
            else:
                batch_idx = range(X_enc.shape[0])
            for batch in batch_idx:
                enc_inp = X_enc[batch, :, :, :]
                dec_inp = X_dec[batch, :, :, :]
                dec_out = Y_dec[batch, :, :]
                train_step(enc_inp, dec_inp, dec_out)

                '''
                if batch % 50 == 0:
                    print('Epoch {} Batch {} Loss {:.7f}'.format(
                        epoch + 1, batch, self.train_loss.result()
                    ))
                '''
            if validation_data is not None:
                if shuffle:
                    val_idx = np.random.permutation(validation_data[0].shape[0])
                else:
                    val_idx = range(validation_data[0].shape[0])
                for batch in val_idx:
                    enc_inp = validation_data[0][batch, :, :, :]
                    dec_inp = validation_data[1][batch, :, :, :]
                    dec_out = validation_data[2][batch, :, :]
                    validation_step(enc_inp, dec_inp, dec_out)

                print('Epoch {} Train Loss {:.7f} Validation Loss {:.7f}'.format(epoch + 1,
                                                                                 self.train_loss.result(),
                                                                                 self.val_loss.result()))

                print('Time taken for 1 epoch: {} secs\n'.format(time.time() - stime))

                self.history["train"].append(self.train_loss.result())
                self.history["val"].append(self.val_loss.result())
            else:
                print('Epoch {} Train Loss {:.7f}'.format(epoch + 1,
                                                          self.train_loss.result()))

                print('Time taken for 1 epoch: {} secs\n'.format(time.time() - stime))
                self.history["train"].append(self.train_loss.result())

    def save(self, path):
        if path[-1] != "/":
            path += "/"
        if not os.path.exists(path):
            os.makedirs(path)
        f = open(path + "transformer_params.p", 'wb')

        self.model.save_weights(path + "model_weights.h5")

        dict_to_save = self.__dict__.copy()
        keys_to_remove = ['model','loss_function','train_loss','val_loss','optimizer']

        for key in keys_to_remove:
            dict_to_save.pop(key,None)
        pickle.dump(dict_to_save, f, 2)
        f.close()

        np.save(path + "hist_train.npy", np.array(self.history["train"]))
        np.save(path + "hist_val.npy", np.array(self.history["val"]))

    def load(self, path,shaper_enc,shaper_dec):
        if path[-1] != "/":
            path += "/"
        f = open(path + "transformer_params.p", 'rb')
        tmp_dict = pickle.load(f)
        f.close()
        self.__dict__.update(tmp_dict)

        self.model = TransformerModel(self.num_layers, self.d_model, self.kernel_size, self.num_heads, self.dff,
                                      self.output_size, self.input_seq_len,self.target_seq_len, rate=self.rate)
        self.predict(shaper_enc,shaper_dec)
        self.model.load_weights(path+"model_weights.h5")

class DataPreprocessor():
    def __init__(self, data, label, valsplit, normalizers=[], shuffle=True, equalize=False, remove_duplicates=False):
        self.data = data
        self.label = label
        self.valsplit = valsplit
        self.normalizers = normalizers
        self.shuffle = shuffle
        self.equalize = equalize
        self.remove_duplicates = remove_duplicates

        self.process()

    def process(self):
        if self.remove_duplicates:
            _, to_keep = np.unique(self.data, axis=0, return_index=True)
            self.data = self.data[to_keep, :]
            self.label = self.label[to_keep]

        if self.equalize:
            uniq, cnts = np.unique(self.label, return_counts=True)
            toadd = np.min(cnts)
            equalized_data = np.zeros(
                (len(uniq) * toadd, self.data.shape[1], self.data.shape[2]))
            equalized_labels = np.zeros((len(uniq) * toadd, 1))
            for i in range(len(uniq)):
                index = np.random.permutation(toadd)
                equalized_data[i * toadd:(i + 1) * toadd] = ((
                                                                 self.data[self.label == uniq[i]])[index, :])[:toadd, :]
                equalized_labels[i * toadd:(i + 1) * toadd] = uniq[i]
            self.data = equalized_data.copy()
            self.label = equalized_labels.copy()

        data_size = self.data.shape[0]

        for n in self.normalizers:
            self.data = n.normalize(self.data)

        if self.shuffle:
            idx = np.random.permutation(data_size)
            self.data = self.data[idx, ...]
            self.label = self.label[idx, ...]

        uniq, cnts = np.unique(self.label, return_counts=True)

        '''
        self.sup_label = np.zeros((data_size, uniq))
        for i in range(data_size):
            self.sup_label[i][int(self.label[i])] = 1

        '''

        validx = int(self.valsplit * data_size)

        self.valData = self.data[:validx, ...]
        self.trainData = self.data[validx:, ...]

        self.valLabel = self.label[:validx, ...]
        self.trainLabel = self.label[validx:, ...]

    def generateTimeWindows(self, in_seq_len, tar_seq_len, shift):
        datalen = self.data.shape[1]
        if tar_seq_len > shift + in_seq_len:
            raise ValueError("Target longer than input plus shift.")
        numpersample = datalen - in_seq_len - shift + 1
        if numpersample < 1:
            raise ValueError("Too large windows, no resulting shifted data.")

        trainSamples = self.trainData.shape[0]
        trainDim = self.trainData.shape[2]
        trainInp = np.zeros((trainSamples, numpersample, in_seq_len, trainDim))
        trainTar = np.zeros((trainSamples, numpersample, tar_seq_len, trainDim))

        valSamples = self.valData.shape[0]
        valDim = self.valData.shape[2]
        valInp = np.zeros((valSamples, numpersample, in_seq_len, valDim))
        valTar = np.zeros((valSamples, numpersample, tar_seq_len, valDim))

        for i in range(self.trainData.shape[0]):
            for j in range(numpersample):
                trainInp[i, j, ...] = self.trainData[i, j:in_seq_len + j, ...]
                trainTar[i, j, ...] = self.trainData[i, j + in_seq_len + shift - tar_seq_len:j + in_seq_len + shift,
                                      ...]

        for i in range(self.valData.shape[0]):
            for j in range(numpersample):
                valInp[i, j, ...] = self.valData[i, j:in_seq_len + j, ...]
                valTar[i, j, ...] = self.valData[i, j + in_seq_len + shift - tar_seq_len:j + in_seq_len + shift, ...]

        for n in self.normalizers:
            self.data = n.normalize(self.data)

        return trainInp, trainTar, valInp, valTar

    def generateStructured(self, in_seq_len):
        datalen = self.data.shape[1]
        numpersample = datalen - in_seq_len
        if numpersample < 1:
            raise ValueError("Too large windows, no resulting shifted data.")

        trainSamples = self.trainData.shape[0]
        trainDim = self.trainData.shape[2]
        trainInp = np.zeros((trainSamples, numpersample, in_seq_len, trainDim))
        trainTar = np.zeros((trainSamples, numpersample, trainDim))

        valSamples = self.valData.shape[0]
        valDim = self.valData.shape[2]
        valInp = np.zeros((valSamples, numpersample, in_seq_len, valDim))
        valTar = np.zeros((valSamples, numpersample, valDim))

        for i in range(self.trainData.shape[0]):
            for j in range(numpersample):
                trainInp[i, j, ...] = self.trainData[i, j:in_seq_len + j, ...]
                trainTar[i, j, ...] = self.trainData[i, in_seq_len + j, ...]

        for i in range(self.valData.shape[0]):
            for j in range(numpersample):
                valInp[i, j, ...] = self.valData[i, j:in_seq_len + j, ...]
                valTar[i, j, ...] = self.valData[i, in_seq_len + j, ...]

        return trainInp, trainTar, valInp, valTar

    def denormalize(self, normdata):
        for n in reversed(self.normalizers):
            normdata = n.denormalize(normdata)
        return normdata


class LastDimensionNormalizer:
    def __init__(self, normtype="minmax"):
        self.normtype = normtype

    def normalize(self, data):
        if self.normtype == "minmax":
            self.min = np.min(data.reshape((-1, data.shape[-1])), axis=0)
            self.max = np.max(data.reshape((-1, data.shape[-1])), axis=0)
            return (data - self.min) / (self.max - self.min)

        if self.normtype == "standard":
            self.mu = np.average(data.reshape((-1, data.shape[-1])), axis=0)
            self.std = np.std(data.reshape((-1, data.shape[-1])), axis=0)

            return (data - self.mu) / self.std

    def denormalize(self, data):
        if self.normtype == "minmax":
            return data * (self.max - self.min) + self.min
        if self.normtype == "standard":
            return data * self.std + self.mu


class Batcher:
    def __init__(self, batch_size, shuffle=False):
        self.shuffle = shuffle
        self.shuffleOrder = None
        self.batch_size = batch_size

    def batch(self, data, reorder=False):
        if self.shuffle:
            if reorder == True or self.shuffleOrder is None:
                self.shuffleOrder = np.random.permutation(data.shape[0])

            data = data[self.shuffleOrder, ...]

        batch_num = data.shape[0] // self.batch_size
        rem = data.shape[0] % self.batch_size

        if rem == 0:
            return np.array(np.vsplit(data, batch_num))
        else:
            return np.array(np.vsplit(data[:-rem, ...], batch_num))