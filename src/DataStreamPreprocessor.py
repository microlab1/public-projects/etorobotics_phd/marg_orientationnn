import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

from tensorflow.python.data.ops import dataset_ops
from tensorflow.python.ops import array_ops
from tensorflow.python.ops import math_ops
#from tensorflow.python.util.tf_export import keras_export

from Quaternion_functions import MOC_to_MAD, MAD_to_MOC, transform, turn, distance_check, mocap_correction
import ahrs


class DataStream:
    def __init__(self, folder_name):
        self.folder_name = folder_name
        
        self.marg_df  = pd.read_csv(self.folder_name + "Marg.csv")
        self.mocap_df = pd.read_csv(self.folder_name + "Mocap.csv")
        
        self.data = np.array([])
        self.data_header = []
        
        self.mag_offset = np.array([0.0, 0.0, 0.0])
        self.mag_calibrated = False
        
        self.resamplingData()
        
        self.transform_quat = []
        self.transform_calculated = False
        self.axis = 'none'

    def plotData(self, plot3D=False, plot2D=False, acc=False, gyro=False, mag=False, magCalib=False, quat=False, rot=False, cols=[], interval=[]):
        if interval == []:
            start = 0
            end = self.data.shape[0]
        else:
            start = interval[0]
            end = interval[1]
          
        if plot3D == True:
            plt.figure(1)
            ax = plt.axes(projection='3d')
            ax.scatter3D(self.data[start:end,10], self.data[start:end,12], self.data[start:end,11], cmap='Greens')
            ax.set_xlabel('MoCap_X')
            ax.set_ylabel('MoCap_Z')
            ax.set_zlabel('MoCap_Y')
            ax.set_title('3D path');
            ax.view_init(10, 10)
            plt.show()
        
        if plot2D == True:     

            plt.figure(2, figsize=(15, 3))
            
            plt.subplot(131)
            plt.title('2D plot X-Y')
            plt.plot(self.data[start:end,10], self.data[start:end, 11], color='blue')
            plt.xlabel('MoCap_X [mm]')
            plt.ylabel('MoCap_Y [mm]')
            
            plt.subplot(132)
            plt.title('2D plot Z-X')
            plt.plot(self.data[start:end,12], self.data[start:end, 10], color='blue')
            plt.xlabel('MoCap_Z [mm]')
            plt.ylabel('MoCap_X [mm]')
            
            plt.subplot(133)
            plt.title('2D plot Y-Z')
            plt.plot(self.data[start:end,11], self.data[start:end, 12], color='blue')
            plt.xlabel('MoCap_Y [mm]')
            plt.ylabel('MoCap_Z [mm]')
            
            plt.show
            
        if acc == True:
            plt.figure(3, figsize=(15, 3))
            
            plt.subplot(131)
            plt.title("Acceleration X")
            plt.plot(self.data[start:end,0], self.data[start:end,1], color='blue', label='AccX')
            plt.xlabel('Time [s]')
            plt.ylabel('Acceleration [m/s^2]')
        
            plt.subplot(132)
            plt.title("Acceleration Y")
            plt.plot(self.data[start:end,0], self.data[start:end,2], color='green', label='AccY')
            plt.xlabel('Time [s]')
        
            plt.subplot(133)
            plt.title("Acceleration Z")
            plt.plot(self.data[start:end,0], self.data[start:end,3], color='red', label='AccZ')
            plt.xlabel('Time [s]')
    
            plt.show()
        
        if gyro == True:
            plt.figure(4, figsize=(15, 3))
            
            plt.subplot(131)
            plt.title("Gyroscope X")
            plt.plot(self.data[start:end,0], self.data[start:end,4], color='blue', label='AccX')
            plt.xlabel('Time [s]')
            plt.ylabel('Speed [°/s]')
        
            plt.subplot(132)
            plt.title("Gyroscope Y")
            plt.plot(self.data[start:end,0], self.data[start:end,5], color='green', label='AccY')
            plt.xlabel('Time [s]')
        
            plt.subplot(133)
            plt.title("Gyroscope Z")
            plt.plot(self.data[start:end,0], self.data[start:end,6], color='red', label='AccZ')
            plt.xlabel('Time [s]')
                        
            plt.show()
            
        if mag == True:
            plt.figure(5, figsize=(15, 3))
            
            plt.subplot(131)
            plt.title("Magneto X")
            plt.plot(self.data[start:end,0], self.data[start:end,7], color='blue', label='AccX')
            plt.xlabel('Time [s]')
            plt.ylabel('Compass [uG]')

            plt.subplot(132)
            plt.title("Magneto Y")
            plt.plot(self.data[start:end,0], self.data[start:end,8], color='green', label='AccY')
            plt.xlabel('Time [s]')

            plt.subplot(133)
            plt.title("Magneto Z")
            plt.plot(self.data[start:end,0], self.data[start:end,9], color='red', label='AccZ')
            plt.xlabel('Time [s]')
            plt.show()
            
        if magCalib == True:
            plt.figure(6, figsize=(15, 4))
            
            plt.subplot(131)
            plt.title('Mag X-Y')
            plt.plot( self.data[start:end,7], self.data[start:end,8], color='blue')
            plt.xlabel('Mag X')
            plt.ylabel('Mag Y')
            
            plt.subplot(132)
            plt.title('Mag X-Z')
            plt.plot( self.data[start:end,7], self.data[start:end,9], color='blue')
            plt.xlabel('Mag X')
            plt.ylabel('Mag Z')
            
            plt.subplot(133)
            plt.title('Mag Y-Z')
            plt.plot( self.data[start:end,8], self.data[start:end,9], color='blue')
            plt.xlabel('Mag Y')
            plt.ylabel('Mag Z')
            
            plt.show()
         
        if quat == True:
            plt.figure(7)
            plt.title('MoCap quaternions')
            plt.plot(self.data[start:end,0], self.data[start:end, 13], color='blue', label='quatW')
            plt.plot(self.data[start:end,0], self.data[start:end, 14], color='red', label='quatY')
            plt.plot(self.data[start:end,0], self.data[start:end, 15], color='green', label='quatZ')
            plt.plot(self.data[start:end,0], self.data[start:end, 16], color='orange', label='quatX')
            plt.xlabel('Time [ms]')
            plt.ylabel('Quaternions [-]')
            plt.legend()
            plt.show()
            
        if rot == True:
            plt.figure(8)
            plt.title('MoCap Eulers')
            plt.plot(self.data[start:end,0], self.data[start:end, 17], color='blue', label='rotX')
            plt.plot(self.data[start:end,0], self.data[start:end, 18], color='red', label='rotY')
            plt.plot(self.data[start:end,0], self.data[start:end, 19], color='green', label='rotZ')
            plt.xlabel('Time [ms]')
            plt.ylabel('Eulers [-]')
            plt.legend()
            plt.show()
            
        if cols != []:
            plt.figure(9)
            for col in cols:
                plt.plot(self.data[start:end,0], self.data[start:end, col], label=self.data_header[col])
            plt.xlabel('Time [ms]')
            plt.ylabel('[-]')
            plt.legend()
            plt.show()
 
    def resamplingData(self):
        ''' 
        This function merge the MARG data with the MoCap data by interpolating the missing data
        resampling according to the MARG sensors. The interpolation is linear. Data point out of 
        syncronization will be discarded.
        '''
        marg_header  = self.marg_df.columns.values.tolist()
        mocap_header = self.mocap_df.columns.values.tolist()[1:]
        self.data_header = marg_header + mocap_header
        
        arr_mess = self.marg_df.copy().to_numpy()
        arr_mocap = self.mocap_df.copy().to_numpy()
        arr_mocap = np.delete(arr_mocap, 0, 0)  # timestamp of the first 2 rows are the same

        # Delete rows, that can be matcheed with MoCap data (out of range)
        delete_row = []
        for i in range(0, arr_mess.shape[0]):
            if arr_mess[i, 0] < arr_mocap[0, 0]:
                delete_row.append(i)
            elif arr_mess[i, 0] > arr_mocap[-1, 0]:
                delete_row.append(i)

        arr_mess = np.delete(arr_mess, np.array(delete_row), 0)

        extension = np.empty((arr_mess.shape[0], arr_mocap.shape[1] - 1,)) # mocap timestamp not needed
        extension[:] = np.nan

        arr_mess = np.hstack((arr_mess, extension))

        idx = 0
        for row in range(arr_mess.shape[0]):
            if arr_mess[row, 0] > arr_mocap[idx+1, 0]:
                idx = idx + 1

            for col in range(arr_mocap.shape[1] - 1):
                arr_mess[row, col + 10] = np.interp(arr_mess[row, 0], [arr_mocap[idx, 0], arr_mocap[idx + 1, 0]],
                                                    [arr_mocap[idx, col + 1], arr_mocap[idx + 1, col + 1]])

        arr_mess[:, 0] = arr_mess[:, 0] - arr_mess[0, 0]
        self.data = arr_mess.astype(np.float32)
        
        temp_header = self.data_header[16]
        self.data_header[14:17] = self.data_header[13:16] 
        self.data_header[13] = temp_header 
        
        temp_W = self.data[:, 16].copy()
        self.data[:,14:17] = self.data[:,13:16]
        self.data[:,13] = temp_W 

    def keepData(self, start, end):
        '''keep the desired amount of data.'''
        self.data = self.data[start:end, :]
    
    def smooth(self, x, window_len, window):
        # https://scipy-cookbook.readthedocs.io/items/SignalSmooth.html
        s=np.r_[x[window_len-1:0:-1],x,x[-2:-window_len-1:-1]]
        #print(len(s))
        if window == 'flat': #moving average
            w=np.ones(window_len,'d')
        else:
            w=eval('np.'+window+'(window_len)')
                
            y=np.convolve(w/w.sum(),s,mode='valid')
        return y
            
    def filterData(self, window_len, window_type, filter_list):
        corr = int(window_len/2)
        
        for elem in filter_list:
            a = self.data[:, elem]
            self.data[:, elem] = self.smooth(a, window_len, window_type)[corr:(corr-window_len)+1]
           

    def timeseries_dataset_from_array(
    # reference: https://github.com/tensorflow/tensorflow/blob/v2.3.0/tensorflow/python/keras/preprocessing/timeseries.py#L29-L199
        self,
        sequence_length,
        sequence_stride=1,
        sampling_rate=1,
        batch_size=128,
        shuffle=False,
        seed=None,
        start_index=None,
        end_index=None):

        data = self.data
        targets = None
        
        # Validate the shape of data and targets
        if targets is not None and len(targets) != len(data):
            raise ValueError('Expected data and targets to have the same number of '
                                             'time steps (axis 0) but got '
                                             'shape(data) = %s; shape(targets) = %s.' %
                                             (data.shape, targets.shape))
        if start_index and (start_index < 0 or start_index >= len(data)):
            raise ValueError('start_index must be higher than 0 and lower than the '
                                             'length of the data. Got: start_index=%s '
                                             'for data of length %s.' % (start_index, len(data)))
        if end_index:
            if start_index and end_index <= start_index:
                raise ValueError('end_index must be higher than start_index. Got: '
                                                 'start_index=%s, end_index=%s.' %
                                                 (start_index, end_index))
            if end_index >= len(data):
                raise ValueError('end_index must be lower than the length of the data. '
                                                 'Got: end_index=%s' % (end_index,))
            if end_index <= 0:
                raise ValueError('end_index must be higher than 0. '
                                                 'Got: end_index=%s' % (end_index,))
    
        # Validate strides
        if sampling_rate <= 0 or sampling_rate >= len(data):
            raise ValueError(
                    'sampling_rate must be higher than 0 and lower than '
                    'the length of the data. Got: '
                    'sampling_rate=%s for data of length %s.' % (sampling_rate, len(data)))
        if sequence_stride <= 0 or sequence_stride >= len(data):
            raise ValueError(
                    'sequence_stride must be higher than 0 and lower than '
                    'the length of the data. Got: sequence_stride=%s '
                    'for data of length %s.' % (sequence_stride, len(data)))
    
        if start_index is None:
            start_index = 0
        if end_index is None:
            end_index = len(data)
    
        # Determine the lowest dtype to store start positions (to lower memory usage).
        num_seqs = end_index - start_index - (sequence_length * sampling_rate) + 1
        if num_seqs < 2147483647:
            index_dtype = 'int32'
        else:
            index_dtype = 'int64'
    
        # Generate start positions
        start_positions = np.arange(0, num_seqs, sequence_stride, dtype=index_dtype)
        if shuffle:
            if seed is None:
                seed = np.random.randint(1e6)
            rng = np.random.RandomState(seed)
            rng.shuffle(start_positions)
    
        sequence_length = math_ops.cast(sequence_length, dtype=index_dtype)
        sampling_rate = math_ops.cast(sampling_rate, dtype=index_dtype)
    
        positions_ds = dataset_ops.Dataset.from_tensors(start_positions).repeat()
    
        # For each initial window position, generates indices of the window elements
        indices = dataset_ops.Dataset.zip(
                (dataset_ops.Dataset.range(len(start_positions)), positions_ds)).map(
                        lambda i, positions: math_ops.range(    # pylint: disable=g-long-lambda
                                positions[i],
                                positions[i] + sequence_length * sampling_rate,
                                sampling_rate),
                        num_parallel_calls=dataset_ops.AUTOTUNE)
    
        dataset = self.sequences_from_indices(data, indices, start_index, end_index)
        if targets is not None:
            indices = dataset_ops.Dataset.zip(
                    (dataset_ops.Dataset.range(len(start_positions)), positions_ds)).map(
                            lambda i, positions: positions[i],
                            num_parallel_calls=dataset_ops.AUTOTUNE)
            target_ds = self.sequences_from_indices(
                    targets, indices, start_index, end_index)
            dataset = dataset_ops.Dataset.zip((dataset, target_ds))
        if shuffle:
            # Shuffle locally at each iteration
            dataset = dataset.shuffle(buffer_size=batch_size * 8, seed=seed)
        dataset = dataset.batch(batch_size, drop_remainder=True)
        dataset2 = np.stack(list(dataset))
        return dataset2


    def sequences_from_indices(self, array, indices_ds, start_index, end_index):
        '''Create a sequence from data.'''
        # reference: https://github.com/tensorflow/tensorflow/blob/v2.3.0/tensorflow/python/keras/preprocessing/timeseries.py#L29-L199
        dataset = dataset_ops.Dataset.from_tensors(array[start_index : end_index])
        dataset = dataset_ops.Dataset.zip((dataset.repeat(), indices_ds)).map(
                lambda steps, inds: array_ops.gather(steps, inds),    # pylint: disable=unnecessary-lambda
                num_parallel_calls=dataset_ops.AUTOTUNE)
        return dataset

    def addFeature(self, feature_list):
        '''
        Add an extra feature column to the data array.
        '''
        for name in feature_list:
            if (name[0:5] == 'diff_') and (name[5:] in self.data_header):
                col = self.data_header.index(name[5:])
                diff = np.zeros((self.data.shape[0], 1))
                diff[0] = 0
                diff[1:] = np.diff(self.data[:,col]).reshape(-1,1)
                self.data = np.hstack([self.data, diff])
                self.data_header.append(name)
                
            elif (name[0:5] != 'diff_') and (name in self.data_header):
                col = self.data_header.index(name)
                self.data = np.hstack([self.data, self.data[:, col].reshape(-1,1)])
                self.data_header.append(name)
                
            else:
                print('Wrong feature name!')
                break 
    
    def dataParser(self, inputs, labels, sequence_length, const_inputs = []):
        '''
        Transform data array into time series sequence tensor
        '''
        matrix = self.timeseries_dataset_from_array(
            sequence_length=sequence_length, sequence_stride=1, sampling_rate=1,
            batch_size=1, shuffle=False, seed=None, start_index=None, end_index=None
            ).astype(np.float32)
        
        matrix = matrix.reshape((matrix.shape[0],matrix.shape[2],matrix.shape[3]))
        
        # placeholder for input (X) and output (Y) array and colum index
        X = np.zeros((matrix.shape[0],matrix.shape[1],len(inputs))).astype(np.float32)
        Y = np.zeros((matrix.shape[0],matrix.shape[1],len(labels))).astype(np.float32)
        x_id = 0
        y_id = 0
        X_header = []
        Y_header = []
        
        # Build input feature array as X
        for i in inputs:
            X[:,:,x_id] = matrix[:,:, i]
            x_id = x_id+1
            X_header.append(self.data_header[i])
            
        # Build label (desired output) feature array as Y
        for j in labels:
            Y[:,:,y_id] = matrix[:,:, j]
            y_id = y_id+1
            Y_header.append(self.data_header[j])
            
        # In the input (X) array feautures can be constant in every sequence
        for k in range(X.shape[0]):
            for s in const_inputs:
                X[k, :, s] = X[k, 0, s].copy()
        
        
        return X, X_header, Y, Y_header
    
    def showHeader(self):
        ''' 
        This function gets the header infos, so user can define the desired output according to it
        '''
        for i in range(len(self.data_header)):
            print(i,'-', self.data_header[i])
        
    def calibrateMag(self, dataStream):
        '''
        This function calibrates the Magneto sensor offset from an independent calibration DataStream. 
        The used measurement should be made by rotating the sensor around every axis multiple time. 
        '''
        if self.mag_calibrated == False:
            self.mag_offset[0] =(np.max(dataStream.data[:,7])+np.min(dataStream.data[:,7]))/2
            self.mag_offset[1] =(np.max(dataStream.data[:,8])+np.min(dataStream.data[:,8]))/2
            self.mag_offset[2] =(np.max(dataStream.data[:,9])+np.min(dataStream.data[:,9]))/2
            
            self.data[:,7]  = self.data[:,7] - self.mag_offset[0]
            self.data[:,8]  = self.data[:,8] - self.mag_offset[1]
            self.data[:,9]  = self.data[:,9] - self.mag_offset[2]
        
        self.mag_calibrated = True
    
#================================quat funcions=======================================
    def calcMadgwick(self):
        '''Calculate the Madgwick predictions.'''
        Acc    = self.data[:,1:4]
        Gyro   = self.data[:,4:7]
        Mag    = self.data[:,7:10]
            
        MadgwickQ = ahrs.filters.Madgwick(acc=Acc*9.8077, gyr=Gyro/57.3, mag=Mag*10, gain=0.000001, Dt=0.004)
        
        self.data = np.hstack([self.data, MadgwickQ.Q])
        self.data_header.append('Madgwick_W')
        self.data_header.append('Madgwick_X')
        self.data_header.append('Madgwick_Y')
        self.data_header.append('Madgwick_Z')
        

    def calibMadgwick(self, d_stand, axis, plot_distance = False):
        '''Calculates the quaternion needed to transform the madgwick to mocaps.'''
        
        if (self.transform_calculated == False or self.axis != axis):
            
            Acc    = d_stand.data[:,1:4]
            Gyro   = d_stand.data[:,4:7]
            Mag    = d_stand.data[:,7:10]
            MocapQ = d_stand.data[:,13:17]
            
            MadgwickQ = ahrs.filters.Madgwick(acc=Acc*9.8077, gyr=Gyro/57.3, mag=Mag*10, gain=0.000001, Dt=0.004)

            if (axis == 'MAD'):
                self.axis = 'MAD'
                # madgwick tengelyeit fogjuk forgatni
                MadgwickQ.Q = MAD_to_MOC(MadgwickQ.Q) #w,x,y,z -> w,y,x,z
                
                #checking distance
                distance = distance_check(MocapQ, MadgwickQ.Q, dlist = True, plot = False)
                #mocap quat correction
                if (max(distance) > 2):
                    MocapQ = mocap_correction(MocapQ, plot=False)
                distance_check(MocapQ, MadgwickQ.Q, dlist = False, plot = plot_distance)
                
                solution = transform(MocapQ, MadgwickQ.Q, axis="MAD", n=len(MocapQ))
                #print ('the transforming quaternion =', solution) #esetleg ki is lehet írni
                self.transform_quat=solution
                self.transform_calculated = True

            elif (axis == 'MOC'):
                self.axis = 'MOC'
                MocapQ = MOC_to_MAD(MocapQ) #w,x,y,z -> w,y,x,z
                #checking distance
                distance = distance_check(MocapQ, MadgwickQ.Q, dlist = True, plot = False)
                #mocap quat correction
                if (max(distance) > 2):
                    MocapQ = mocap_correction(MocapQ, plot=False)
                distance_check(MocapQ, MadgwickQ.Q, dlist = False, plot = plot_distance)
                
                solution = transform(MadgwickQ.Q, MocapQ, axis="MAD", n=len(MocapQ))
                self.transform_quat=solution
                self.transform_calculated = True
            else:
                print('Wrong axis!')
    
    def mocapToMadgwick(self, lst):
        '''Rotate Mocap quaternion to Madgwick coordinate system.'''  
        if (self.transform_calculated == True):
            if (self.axis == 'MOC'):
                MocapQ = self.data[:,lst]
                MocapQ = MOC_to_MAD(MocapQ)
                
                for i in range(len(self.data)): # quaterniok átfordítása
                    #q10=turn(self.transform_quat, self.data[i,lst], axis ="MAD") # fogató quat, mit forgat, axis = melyiknek forgattuk a tengelyeit
                    q10=turn(self.transform_quat, MocapQ[i], axis ="MOC")                    
                    self.data[i,lst]=q10
            else:
                print('Wrong axis.')
        else:
            print('Transform quaternion is missing.')
            
    def madgwickToMocap(self, lst):
        ''' Rotate Madgwick quaternion to Mocap coordinate system.'''
        if (self.transform_calculated == True):
            if (self.axis == 'MAD'):
                MadgwickQ = self.data[:,lst]
                MadgwickQ = MAD_to_MOC(MadgwickQ)
                
                for i in range(len(self.data)): # quaterniok átfordítása
                    q10=turn(self.transform_quat, MadgwickQ[i], axis ="MAD")
                    self.data[i,lst]=q10
            else:
                print('Wrong axis')
        else:
            print('Transformation quaternion is missing')
#=================================quat function end=============================
        
class LastDimensionNormalizer:
    def __init__(self, normtype="minmax"):
        self.normtype = normtype

    def normalize(self, data):
        if self.normtype == "minmax":
            self.min = np.min(data.reshape((-1,data.shape[-1])), axis=0)
            self.max = np.max(data.reshape((-1,data.shape[-1])), axis=0)
            return (data - self.min) / (self.max - self.min)

        if self.normtype == "standard":
            self.mu = np.average(data.reshape((-1,data.shape[-1])),axis=0)
            self.std = np.std(data.reshape((-1,data.shape[-1])),axis=0)

            return (data-self.mu)/self.std

    def denormalize(self, data):
        if self.normtype == "minmax":
            return data * (self.max - self.min) + self.min
        if self.normtype == "standard":
            return data * self.std + self.mu


class Batcher:
    def __init__(self, batch_size, shuffle=False):
        self.shuffle = shuffle
        self.shuffleOrder = None
        self.batch_size = batch_size

    def batch(self, data, reorder=False):
        if self.shuffle:
            if reorder == True or self.shuffleOrder is None:
                self.shuffleOrder = np.random.permutation(data.shape[0])

            data = data[self.shuffleOrder, ...]

        batch_num = data.shape[0] // self.batch_size
        rem = data.shape[0] % self.batch_size

        if rem == 0:
            return np.array(np.vsplit(data, batch_num))
        else:
            return np.array(np.vsplit(data[:-rem,...],batch_num))

