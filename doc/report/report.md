# My work through the semester

My work throug the semester could be distributed into three parts: <br>
- `Convolutional neural network constructing and testing`: Constructing and testing convolutional neural networks for the task of processing the data provided by the MARG sensor
- `Working with quaternions and the Madgwick algorithm`: processing the quaternions provided by the Motion capture system, and the quaternions calculated with the Madgwick algorithm, in order to make them comparable
- `Testing the transformer network`: testing the transformer network with various parameters in order to train the network to be able to process the MARG sensor's data better than the madgwick algorithm 

### Convolutional neural network constructing and testing:

First I tried to construct convolutional neural networks in order to process the data from the MARG sensor in order to calculate the position of the sensor. I tried a couple of structures with a few and with more layers as well, but it seemed a regular neural network can't really do this task. In the end the attention of project turned to a transformer neural network, therefore working with convolutional networks wasn't really needed anymore.

### Working with quaternions and the Madgwick algorithm:

With the idea of using a transformer neural network, the idea of calculating the rotation of the sensor via neural network came as well. To test how good a neural network could be in such a task we wanted to use the Madgwick algorithm for a reference. The rotation in this case is represented by quaternions. My task was to somehow turn the quaternions calculated by the Madgwick algorith accordint to the data of the MARG sensor into the quaternions captured by the motion capture system. At first it seemed a quite difficult task, so I needed to understand how quterninos work. After some research I came across a possible solution, but it didn't really turned in the way we wanted. The basis of the solution was found only because of a very practical measurement data. On this data it was visible, that with some changes in the axles, the two type of quaternions could be matched. <br>
Finally with some promising reuslts the Quaternion_functions.py script was born, collecting the base functions need to make a quaternion turn between the motion capture and madgwick qauternions. These functions include for example a function that calculates the quaternion wich serves as a multiplier in the process of the quaternion turning, another example is a function that calculates the quaternion distance between the two types of quaternions, due to the fact, that this distance need to be roughly constant, for the quaternionturn to work. <br>
The quaternion functions seemed to be working fine, so for simpler use, we integrated it into the DataStreamPreprocess.py script, which is kind of the base of all this whole project. It seems that the intagration is now almost complete, the only thing which is left to do is maybe adjusting the manual for the script which will certanly be done soon.

### Testing the transformer network:

With having the reference madgwick quaternions, we had the chance to test the new transformer neural network. I tried to optimalize the hiper parameters of the transformer network, and actually I received promising results, unfortunately though it turned out, that the good result may have been the consequence of a theoritical mistake. After realising this, we had to use the network with other inputs, and with that change the network couldn't really perform that good, so in conclusion maybe another hiper parameter optimalisation is needed <br>
The results of the network with the theoritical mistake with the original motion caption rotation, and the rotation from the madgwick algorithm:

<img src="/doc/pics/original.png">
<img src="/doc/pics/madgwick.png">
<img src="/doc/pics/transform.png">

## Plans for the future:

Plans for the future definetely include more tests with the transformer neural network. I would like to test the now theoritically correct network, whether it has any potentional, and also there is an idea that we should use the network together with the madgwick algorithm to make a better working algorithm.

