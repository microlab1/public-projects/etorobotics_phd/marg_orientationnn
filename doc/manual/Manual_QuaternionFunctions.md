<img src="/doc/pics/GPK_BME_MOGI.png">

# Quaternion_functions.py

[[_TOC_]]

### quaternion = `w_flip(quaternion)` <br>
Changes the order of the components of the quaternion: like this (x,y,z,w) -> (w,x,y,z)
- `quaternion`: (list) A list or nparray with the shape of (n by 4)
-------
### quaternion = `w_back_flip(quaternion)` <br>
Changes the order of the components of the quaternion: like this (w,x,y,z) -> (x,y,z,w)
- `quaternion`: (list) A list or nparray with the shape of (n by 4)
-------
### quaternion = `MOC_to_MAD(quaternion)` <br>
Changes the order of the components of the Mocap quaternion,to make the quaternion distance between the Mocap quaternions and the Madgwick quaternions approximately constant: like this (w,x,y,z) -> (w,y,-x,-z) 
- `quaternion`: (list) A list or nparray with the shape of (n by 4)
-------
### quaternion = `MAD_to_MOC(quaternion)` <br>
Changes the order of the components of the Madgwick quaternion,to make the quaternion distance between the Mocap quaternions and the Madgwick quaternions approximately constant: like this (w,x,y,z) -> (w,-y,x,-z) 
- `quaternion`: (list) A list or nparray with the shape of (n by 4)
-------
### QuaternionProduct = `qprod(a, b)` <br>
Calculates the quaternion product of two quternions: like this a(b)
- `a`: (list) A list or nparray with the shape of (4), with the order of (w,x,y,z)
- `b`: (list) A list or nparray with the shape of (4), with the order of (w,x,y,z)
-------
### transform = `transform(quat1, quat2, axis, n)` <br>
Calculates the transformation quaternion which turns quat2 into quat1 
- `quat1`: (list) A list or nparray with the shape of (n by 4), with the order of (w,x,y,z)
- `quat2`: (list) A list or nparray with the shape of (n by 4), with the order of (w,x,y,z)
- `axis`: (str) either "MAD" or "MOC", determining whether the Madgwick or the Mocap quaternion was changed before with the MAD_to_MOC or with the MOC_to_MAD function
- `n`: (int) the number of quaternions, and the iterations whisch's average determines the transformer quaternion
-------
### mocap = `turn(transform, madgwick, axis)` <br>
turns the madgwick quaternion into mocap quaternion
- `transform`: (list) A list or nparray with the shape of (4), with the order of (w,x,y,z)
- `madgwick`: (list) A list or nparray with the shape of (n by 4), with the order of (w,x,y,z)
- `axis`: (str) either "MAD" or "MOC", determining whether the Madgwick or the Mocap quaternion was changed before with the MAD_to_MOC or with the MOC_to_MAD function
-------
### `distance_check(quat1, quat2)` <br>
makes a plot, which depicts the distance of each quaterion of the two given quaternion vectors
- `quat1`: (list) A list or nparray with the shape of (n by 4), with the order of (w,x,y,z)
- `quat2`: (list) A list or nparray with the shape of (n by 4), with the order of (w,x,y,z)
-
-
-------
### distance = `distance(quat1, quat2)` <br>
calculates the distance between two given quaternions
- `quat1`: (list) A list or nparray with the shape of (4), with the order of (w,x,y,z)
- `quat2`: (list) A list or nparray with the shape of (4), with the order of (w,x,y,z)
- `dlist`: (bool) If true, the function returns a list filled, wtih the distance values
- `plot` : (bool) If true, the function will plot the distances
-------
### mocapQ = `mocap_correction(MocapQ)` <br>
corrects the sudden changes in the quaternion depiction of the orieantaion, which occurs sometimes in the Mocap quaternion
- `quat`: (list) A list or nparray with the shape of (n by 4), with the order of (w,x,y,z)
- `plot`: (bool) If true, the function will plot th distances between the quaternions
-------
### alfa, beta, gamma = `quaternion_to_euler_angle_vectorized1(quat)` <br>
Transforms the rotation from quaternion depiction to Euler depiction
- `quat`: (list) A list or nparray with the shape of (n by 4), with the order of (w,x,y,z)




