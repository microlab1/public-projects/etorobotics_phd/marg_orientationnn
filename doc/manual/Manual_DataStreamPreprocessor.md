<img src="/doc/pics/GPK_BME_MOGI.png">

# DataStreamPreprocessor.py 

The toolset implemented in [**DataStreamPreprocessor.py**](https://gitlab.com/microlab1/student-projects/23_marg_project/-/blob/master/src/MARG_module/src/Preprocess/DataStreamPreprocessor.py) is designed to load MoCap and MARG data from syncronised measurement and provide a useful toolkit to plot and preprocess the data.

[[_TOC_]]

## DataStream

### `d = DataStream(folder_name)` <br>
Load measurement files and create a DataStream. <br>
- `folder_name`: (str) This folder contains the Marg.csv and Mocap.csv files. Example: `folder_name = "../logs/20201002_10_17_42/"` <br> 

Notable variables in the DataStream class: <br>
- `d.data`: Preprocessed data array. <br>
- `d.data_header`: Header information of d.data. <br>       
- `d.mag_offset`: Calculated offset values to calibrate magnetometer offset error. <br>
- `d.mag_calibrated`: True if calibration is done. prevent multiple calibration in the same measurement. <br> 
- `d.transform_quat`: A list contaioning the transformation quaterion in the order of: (w,x,yz) <br>
- `d.transform_calculated`: True if the transformation quaternion was calculated for this dataStream before. Preventing multiple calibrations <br>
- `d.axis`: Determines, whether we turn madgwick into mocap or mocap into madgwick. 'MAD' indicates madgwick to mocap, 'MOC' indicates mocap to madgwick

-------
### `d.keepData(start, end)` <br>
Keep the data between start and end values. <br>
- `start`: (int) Starting index of data to keep<br>
- `end`: (int) Last index of data to keep <br>

-------
### `d.calibrateMag(data_stream)` <br>
Calibrate the magneto sensor offset. <br>
- `data_stream`: (dataStream) Independent DataStream for magneto calibration. <br>

-------
### `d.plotData(plot3D=False, plot2D=False, acc=False, gyro=False, mag=False, magCalib=False, rot=False, interval=[])` <br>
Different plotting options. <br>
- `plot3D=True` - Plot the 3D path from MoCap information (MoCap_X, MoCap_Y, MoCap_Z) <br>
- `plot2D=True` - Plot the 2D path from MoCap information. In the string parameter 2 of the 3 main axis should be chossen from X, Y, Z. Order matters. <br>
- `acc=True` - Plot the accelerometer data from MARG measurement (d.data). All 3 axis included. <br>
- `gyro=True` -  Plot the gyroscope data from MARG measurement (d.data). All 3 axis included. <br>
- `mag=True` -  Plot the magnetometer data from MARG measurement (d.data). All 3 axis included. <br>
- `magCalib=True` - Plot calibration figures for magnetometer data as 2D projections.  <br>
- `quat=True` - Plot MoCap orientation Euler form <br>
- `rot=True` - Plot MoCap orientation quaternions <br> 
- `interval` - (int list) Set the interval for plotting. <br>

-------
### `d.addFeature(feature_list)`
Add extra feature column to the data array inside the DataStream object. <br> 
- `feature_list`: (str list) List containing the new variables from the original data set or with a 'diff_' prefix. <br>

-------
### `d.showHeader()`
Print out the actual header names and column numbers. <br>

-------
### `d.filterData(window_len, window_type, filter_list)`
Filter the desired column of the d.data array <br>
- `window_len`: (int) the size of the filter window. Choosing an odd number is required becuse in this case the filter is symmetric. <br> 
- `window_type`: (string) the type of the filter window. Options: `'flat'`, `'hanning'`, `'hamming'`, `'bartlett'` or `'blackman'`. <br>
- `filter_list`: (int list) the number of columns in a list choosen to be filtered. <br>

Example: Filter the Accelerometer data (all 3 axis) with a 31 measurement wide hanning window. <br>
`d.filterData(31, 'hanning', [2,3,4])` <br>

<img src="/doc/pics/Filters.png"> <br>
[reference](https://scipy-cookbook.readthedocs.io/items/SignalSmooth.html) <br>

-------
### `d.dataParser(inputs, labels, sequence_length, const_inputs = [])`
Parse the data array into labeled time series sequences.
- `inputs`: (int list) Contains the column indexes of the input features in data array. <br>
- `labels`: (int list) Contains the column indexes of the label features in data array. <br>
- `sequence_length`: (int) The length of a time series. <br>
- `const_inputs`: (int list) The column indexes of constant features in input_features. <br>
Return `X, X_header, Y, Y_header` <br>
- `X`: Input data array. <br>
- `X_header`: Header information for X. <br>
- `Y`: Label data array. <br>
- `Y_header`: Header information for Y. <br>
-------
### `d.calcMadgwick()` <br>
Calculates the the rotation in qauternions with the madgwick algortih using the data from the MARG sensor. The function also adds the calculated quaternions to the self.data varialbe <br> 

-------
### `d.calibMadgwick(d_stand, axis, plot_distance)` <br>
calculates the quaternion needed to transform the madgwick quaternions into the mocap quaternions or the mocap quaternions into the madgwick quaterninos. <br>
- `d_stand`: (dataStream) independent dataStream for calculating. <br>
- `plot_distance`: (bool) bool variable determinig whether to the distance between the madgwick and the mocap quaternions. <br>
- `axis`:(str) Determines, whether we turn madgwick into mocap or mocap into madgwick. 'MAD' indicates madgwick to mocap, 'MOC' indicates mocap to madgwick
-------
### `d.madgwickToMocap(lst)` <br>
transforms the dataStream's madgwick type quaternions into mocap quaternions according to the self.transform_quat variable. The transformed quaternions will be saved in the salef.data variables, according the lst variable <br>
- `lst`: (list) contains those columns, which togther make up the madgwick quaternion in the self.data varialbe <br>
-------
### `d.mocapToMadgwick(lst)` <br>
transforms the dataStream's mocap type quaternions into magwick quaternions according to the self.transform_quat variable. The transformed quaternions will be saved in the salef.data variables, according the lst variable <br>
- `lst`: (list) contains those columns, which togther make up the mocap quaternion in the self.data varialbe <br>
-------
## Normalizer
### `nx = LastDimensionNormalizer()`
Create the normalizer class. <br>

-------
### `norm_data = nx.normalize(data)`
Normalize the data. <br>
- `data`: Data array to normalize <br>
-------
## Batcher
### `batcher = Batcher(batch_size, shuffle=False)`
Make batcher class for the data. <br>
- `batch_size`: batch size. <br>
- `shuffle`: Shuffle the batches. Default value is False. <br>
-------
### `data_train = batcher.batch(data, reorder=False)`
Make a batched training dataset. <br>
- `data`: Data input. <br>
- `reorder`: make a random permutation of the data. <br> 
-------
# Possible future work <br>
- calibrate function update: Magneto elipsoid to sphare calibration. <br>
- calibrate function update: Accelerometer calibration. (Use stand still measurement). <br>
- calibrate function update: Gyroscope calibration. (Use stand still measurement). <br>