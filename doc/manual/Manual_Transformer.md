<img src="/doc/pics/GPK_BME_MOGI.png">

# Transformer.py

The [**Transformer.py**](https://gitlab.com/microlab1/student-projects/23_marg_project/-/blob/master/src/Transformer.py) implements the Transformer neural network in a compact form.  <br>

[[_TOC_]]

### `trans = Transformer.Transformer(num_layers, d_model, kernel_size, num_heads, dff, input_size, target_size, output_size, in_seq_len, tar_seq_len, rate=0.1)`
Creates the desired transformer network. <br>
- `num_layers` : Number of paralel layers. <br>
- `d_model`    : Number of neurons in the feed forward fully connected output layer. <br>
- `kernel_size`: Kernel size of the Convolutional layers. <br>
- `num_heads`  : Numeber of attion heads. <br>
- `dff`        : Number of hidden neurons in the feed forward fully connected layer. <br>
- `input_size` : Number of input features on the encoder input.<br>
- `target_size`: Number of input features on the decoder input.<br>
- `output_size`: Number of desired output features at the decoder output.<br>
- `in_seq_len` : Length of input sequence in timesteps on the encoder input. <br>
- `tar_seq_len`: Length of input sequence in timesteps on the decoder input. <br>
- `rate`       : Dropout rate. Default value is 0.1. <br>

### `trans.setLogsparseMasks(local, repetition)`
Create the logsparse mask for the transformer network. <br> 
- `local`     : (int) Size of localy used values for lookback. <br> 
- `repetition`: (int) REpetion of the local lookback. <br>

### `trans.setLoss(loss_function)`
Define loss function for the Transformer network <br>
-`loss_function`: define the loss function <br>
Possible solution: <br>
`def loss_function(real, pred):` <br>
`   return tf.reduce_mean(tf.math.square(real-pred))` <br>

### `trans.setOptimizer(Adam())`
Set the optimiser to Adam for te network. <br>

### `trans.train(epochs,X_enc,X_dec,Y_dec, shuffle = False)`
Train the network. <br>
- `epochs` : Number of epoch in the trainig. <br>
- `X_enc`  : Encode input <br>
- `X_dec`  : Decoder input <br>
- `Y_dec`  : Decoder output <br>
- `shuffle`: Shuffle the batches. Default value False. <br>

### `trans.predict_structured(X_enc,X_dec_init)` 
Make a structured prediction. <br>
- `X_enc`     : Encoder input. Dimensions: samplenum, seq_len, input_dims. <br>
- `X_dec_init`: Decoder input unitial sequence. The prediction will change it continously. Dimensions: 1, seq_len, output_dims <br>