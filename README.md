<img src="/doc/pics/GPK_BME_MOGI.png">

# MARG Project: Orientation and Position estimation with Deep Learning 

The aim of this project to predict orientation and position data from MARG sensor measurement. 

[[_TOC_]]

## Credits<br>
Balázs Nagy, Natababara Gyöngyössy , Richárd Kovács

## Project structure
- [config](https://gitlab.com/microlab1/student-projects/23_marg_project/-/tree/master/config): This folder contains UR3 path files which were used to collect measurement files.
- [doc](https://gitlab.com/microlab1/student-projects/23_marg_project/-/tree/master/doc): This folder contains the documentation in connection with this project. <br>
  - [manual](https://gitlab.com/microlab1/student-projects/23_marg_project/-/tree/master/doc/manual)
    - [Manual_DataStreamPreprocessor](https://gitlab.com/microlab1/student-projects/23_marg_project/-/blob/master/doc/manual/Manual_DataStreamPreprocessor.md)
    - [Manual_QuaternionFunctions](https://gitlab.com/microlab1/student-projects/23_marg_project/-/blob/master/doc/manual/Manual_QuaternionFunctions.md)
    - [Manual_Transformer.md](https://gitlab.com/microlab1/student-projects/23_marg_project/-/blob/master/doc/manual/Manual_Transformer.md)
- [saved_logs](https://gitlab.com/microlab1/student-projects/23_marg_project/-/tree/master/saved_logs): Some example measurement file to test the code. More measurement files are available at [GoogleDrive](https://drive.google.com/drive/folders/1hcXnkmDz6hUwfFZhZn4GS20i_0REK5k4?usp=sharing).
- [src](https://gitlab.com/microlab1/student-projects/23_marg_project/-/tree/master/src): The sourse code of the project. 

## Project desciption

Our goal is to predict orientation and position data from MARG sensor measurement with the help of neural networks. <br>
- Collecting data: <br>
The first step is to collect tha data of a MARG sensor, and also  collect the reference data, which contains the precise position and rotation data. For the project we are using a '1056-PhidgetSpatial 3/3/3' MARG sensor to collect the MARG data, and we provide the reference data via a motion capture camera system, which we use during the measurements. <br>
<img src="/doc/pics/MARG_sensor.jpg">
<br>

- Conventinal algorithm: <br>
In order to compare the results of a neural network we are using the madgwick algorithm, a conventional algorithm. The madgwick algorithm can calculate the rotation of the sensor with the data collected from the sensor. This rotation is in quaterninos, but we have to make an operation on this quaternion to be comperable with the rotation provided by the  motion capture system. After this operation the rotation calculated with the madgwick algorithm is visibly  quite close to the reference rotation, in the beginning at least. <br>
- Neural network:
To challenge the madgwick algorithm we tried 1D convolutional networks at first, but then we had to realise that in order to succeed we need a more complex network, thats why started using a transformer neural netwrok. The transformer neural network showed promising results, but further evaluation is needed in order to make a conclusion. <br>
Some of the results of the network:

<img src="/doc/pics/original.png">
<img src="/doc/pics/madgwick.png">
<img src="/doc/pics/transform.png">

We can also see the effect of the hyper parameters on the network: <br>

<img src="/doc/pics/hyper_parameters.png">

Another option for the neural network is not to challenge the madgwick algorithm, but to develop it. we made some tests to examine this possibility <br>
The results of one our tests: <br>

<img src="/doc/pics/Madgwick_developer_quat0.png">
<img src="/doc/pics/Madgwick_developer_quat1.png">
<img src="/doc/pics/Madgwick_developer_quat2.png">
<img src="/doc/pics/Madgwick_developer_quat3.png">


## References
[cikk neve](https://www.tensorflow.org/tutorials/text/transformer)

[cikk neve](https://arxiv.org/pdf/2001.08317.pdf)
